all: clean build

clean: 
	rm -f from1700.grf

build: clean
	nmlc -c --grf from1700.grf from1700.nml

install: build
	cp -vf from1700.grf ~/.openttd/newgrf
